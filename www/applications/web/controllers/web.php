<?php
/**
 * Access from index.php:
 */
class web_Controller extends ZP_Controller {
	
	public function __construct() {
		$this->app("web");
		$this->Files = $this->core("Files");
		$this->Templates = $this->core("Templates");
		$this->Templates->theme();
		$this->helper("alerts");
		$this->helper("sessions");
		$this->helper("security");
		$this->web_Model = $this->model("web_Model");
		$this->panel_Model = $this->model("panel_Model");
		//$this->panel_Controller = $this->controller("panel_Controller");
	}

	public function index() {
		if( (SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="user") ){
			$this->home();
		}else{
			$this->frontend(); 
		}
	}
	public function frontend(){
		$this->title("Inicio");
		$vars["sliders"]= $this->panel_Model->buscar_todos_informacion();
				
		$vars["view"] = $this->view("inicio",TRUE);
		$this->render("content",$vars);
	}
	public function quienesSomos(){
		$this->title("Quieres Somos");
		$vars["sliders"]= $this->panel_Model->buscar_todos_informacion();
		$vars["view"] = $this->view("informacion",TRUE);
		$this->render("content",$vars);
	}
	public function porqueSubcribirse(){
		$this->title("Porque Subcribirse");
		$vars["sliders"]= $this->panel_Model->buscar_todos_informacion();
		$vars["view"] = $this->view("porque_suscribirse",TRUE);
		$this->render("content",$vars);
	}
	public function queOfrecemos(){
		$this->title("Que ofrecemos");
		$vars["sliders"]= $this->panel_Model->buscar_todos_informacion();
		$vars["view"] = $this->view("que_ofrecemos",TRUE);
		$this->render("content",$vars);
	}
	public function contactanos(){
		$this->title("contactanos");
		$vars["sliders"]= $this->panel_Model->buscar_todos_informacion();
		$vars["view"] = $this->view("contactanos",TRUE);
		$this->render("content",$vars);
	}
	public function home() {	
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			redirect('panel/home');
		}else{
			$this->index(); 
		}
	}
	public function salir() {	
		$logs = $this->web_Model->logs("Salir");		
 		unsetSessions($URL);
		$this->index(); 
	}
	public function perfil_usuario(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="webrio")){
			if(POST("modificar")){
				$uploaddir = '/var/www/web/www/lib/themes/web/images/';
				$uploadfile = $uploaddir.basename($_FILES['userfile']['name']);
				$namefile = basename($_FILES['userfile']['name']);

				if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
					chmod($uploadfile, 0755);
					$data = $this->web_Model->modificar_perfil(SESSION("id"),$namefile);
				
					echo "<script>alert('Datos Guardados con Exito Vuelva a Entrar al Sistema');</script>";
					$logs = $this->web_Model->logs("Modificar Perfil");
					$this->salir();   	
				} else {
					echo "<script>alert('Error en la carga del archivo'".basename($_FILES['userfile']['name'])."')</script>";
					$this->perfil_usuario();
				}


			}else{
				$data = $this->web_Model->buscar_logs(SESSION("ci"));
				$vars["logs"] = $data;
				$this->title("Ver Contactos");
				$vars["view"] = $this->view("perfil",TRUE);
				$this->render("content",$vars);
				$logs = $this->web_Model->logs("Ver Perfil");

			}
		}else{
			$this->login(); 
		}
	}
	public function recuperar_clave(){
			if(POST("registro")){
				$data = $this->web_Model->buscar_pregunta_usuario(POST("correo"));
				if($data[0]["respuesta"]==POST("respuesta")){
					$id=$data[0]["id"];
					$this->web_Model->modificar_clave($id);
					echo "<script>alert('Se ha Modificado Correctamente');</script>";
					$this->login();
				}else{
					echo "<script>alert('La respuesta no es la misma');</script>";
				}
			}else{
				$this->title("Recuperar Clave");
				$vars["view"] = $this->view("recuperar",TRUE);
				$this->render("content",$vars);
			}	
	}
	public function registrar(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			if(POST("registrar_bnt")){
				____(POST("terminos"));
				if(POST("terminos")==FALSE){
					echo "<script>alert('no puedes dejar la nacionalidad en blanco');</script>";
				}else{
					$data = $this->web_Model->registrar();
					echo "<script>alert('Se ha guardado Correctamente');</script>";
					$this->index();
				}
			}else{
				$this->home();	
			}
		}else{
			if(POST("registrar_bnt")){
				if(POST("terminos")==FALSE){
					echo "<script>alert('Tienes que Aceptar los terminos y condiciones del Servicio');</script>";
				}else{
					if((POST("ci")=="")||(POST("nombres")=="")||(POST("apellidos")=="")||(POST("usuario")=="")||(POST("clave")=="")){
						echo "<script>alert('No Puedes Dejar Campos Vacios');</script>";
						$this->index();
					}else{
						$clave = encrypt(POST("clave"),1,TRUE,TRUE);
						$data = $this->web_Model->registrar($clave);
						if($data==FALSE){
							echo "<script>alert('el usuario ya existe');</script>";
						}else{
							echo "<script>alert('Se ha guardado Correctamente');</script>";
						}
						$this->index();
					}
				}
			}else{}
		}
	}
	public function login() {
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			$this->home();
		}else{
			if(POST("entrar_btn")){
				$clave = encrypt(POST("clave"),1,TRUE,TRUE);
				$data = $this->web_Model->login($clave);
				//____($data);
				if($data == false){
					$this->title("Error al Entrar");
					$vars["alert"] = "alerta";
					$this->index();
				}else{
					if($data[0]["estado"]=="bloqueado"){
						echo "<script>alert('Tu Usuario esta Bloqueado Contacta con tu Administrador');</script>";
						echo "<script>document.location.href='".get("webURL")."';</script>";
					}else{
						SESSION("tipo_user", $data[0]["tipo_user"]);
						SESSION("nombre", $data[0]["nombre"]);
						SESSION("apellido", $data[0]["apellido"]);
						SESSION("id", $data[0]["id"]);
						SESSION("fecha_nac", $data[0]["fecha_nac"]);
						SESSION("direccion", $data[0]["direccion"]);
						SESSION("email", $data[0]["email"]);
						SESSION("telefono", $data[0]["telefono"]);
								
						$this->home();
					}
				}
			}else{ }
		}
	}
	
	public function enviar_mensaje_interno() {
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			$this->home();
		}else{
			if(POST("enviar_btn")){
				$data = $this->web_Model->enviar_mensaje_interno();
				//____($data);
				if($data==FALSE){
					echo "<script>alert('Tu mensaje no fue enviado, Intenta mas tarde')</script>";
					$this->contactanos();
				}else{
					echo "<script>alert('Tu mensaje fue enviado, en unas horas te contactaremos')</script>";
					$this->frontend();
				}

				
			}else{ }
		}
	}
	public function buscar_empresa(){
		$vars["sliders"]= $this->panel_Model->buscar_todos_informacion();
		if(POST("enviar_btn")){
			if((POST("estado")=="Elige un Estado")||(POST("buscar")=="Elige un tipo de Busqueda")){
				echo "<script>alert('Debes elegir un Estado y Tipo de busqueda')</script>";
				$this->index();
			}else{
				if(POST("buscar")=="Establecimientos"){
					if(POST("estado")=="Todos"){
						$data = $this->web_Model->buscar_empresa_establecimiento_sin_estado(POST("palabra_clave"));
					}else{
						$data = $this->web_Model->buscar_empresa_establecimiento(POST("estado"),POST("palabra_clave"));
					}
					$this->title("Buscar Empresa");
					$vars["empresas"] = $data;
					$vars["view"] = $this->view("buscar_empresa",TRUE);
					$this->render("content",$vars);

				}elseif(POST("buscar")=="Palabra Clave"){
					if(POST("estado")=="Todos"){
						$data = $this->web_Model->buscar_empresa_palabra_clave_sin_estado(POST("palabra_clave"));
					}else{
						$data = $this->web_Model->buscar_empresa_palabra_clave(POST("estado"),POST("palabra_clave"));
					}
					//____($data);
					$this->title("Buscar Empresa");
					$vars["empresas"] = $this->panel_Model->buscar_todos_empresas_activo();
					$vars["palabras"] = $data;
					$vars["view"] = $this->view("buscar_empresa",TRUE);
					$this->render("content",$vars);
				}else{
					echo "<script>alert('Debes seleccionar un metodo de busqueda')</script>";
					redirect('web/index');
				}
			}
		}else{}
	}
	public function perfil_empresa() {	
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			redirect('panel/home');
		}else{
			$vars["sliders"]= $this->panel_Model->buscar_todos_informacion();
			$vars["empresa"] = $this->panel_Model->buscar_empresa_espesifica($_GET["id_empresa"]);
			$vars["view"] = $this->view("perfil",TRUE);
			$this->render("content",$vars);	 
		}
	}
	
}