			<form action="<?php print path("web/enviar_mensaje_interno"); ?>" method="post" role="form" class="form-horizontal" id="frmcontactanos">
				<h3 align="center">Contactanos</h3>		
				</br>
				<input class="form-control" name="nombres" type="text" placeholder="Nombre Completo" required="required">
				</br>
				<input class="form-control" name="email"  type="Email" placeholder="Correo Electronico" required="required">
				</br>
				<input class="form-control" name="telefono" type="phone" placeholder="Número Telefónico" required="required">			
				</br>
				<select class="form-control" name="estado">
	              <option>Elige un Estado</option>
	              <option>Amazonas</option>
	              <option>Anzoategui</option>
	              <option>Apure</option>
	              <option>Aragua</option>
	              <option>Barinas</option>
	              <option>Bolivar</option>
	              <option>Carabobo</option>
	              <option>Cojedes</option>
	              <option>Delta Amacuro</option>
	              <option>Distrito Capital</option>
	              <option>Falcon</option>
	              <option>Guarico </option>
	              <option>Lara</option>
	              <option>Merida</option>
	              <option>Monagas</option>
	              <option>Nueva Esparta</option>
	              <option>Portuguesa</option>
	              <option>Sucre</option>
	              <option>Tachira</option>
	              <option>Trujillo</option>
	              <option>Vargas</option>
	              <option>Yaracuy</option>
	              <option>Zulia</option>
	            </select>			
				</br>
				<textarea class="form-control" name="mensaje" placeholder="Deje su mensaje" required="required"></textarea>  
				</br>
				<input type="submit" class="btn btn-lg btn-primary btn-block large" name="enviar_btn" value="Enviar Mensaje" />               
			</form>