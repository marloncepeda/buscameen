 <div id="page-wrapper">
   
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="entypo-user fa-fw"></i> Datos de la Empresa <?php print $empresa[0]["nombre_empresa"]; ?></a>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <form action="<?php print $href; ?>" method="post" enctype="multipart/form-data">
                                <fieldset>
                                  <div class="col-lg-3"></div>
                                      <img class="col-lg-3" src="<?php print $this->themePath; ?>/img/logos_empresas/<?php print $empresa[0]["logo"]; ?>" width="300px" height="200px"></img>
                                      <img class="col-lg-3"src="<?php print $this->themePath; ?>/img/mapas_empresas/<?php print $empresa[0]["mapa"]; ?>" width="300px" height="200px"></img>
                                  
                                  <br />
                                  <div class="form-group">
                                      <input class="form-control" name="nombres" pattern="[a-zA-Z]+" type="text" value="<?php print $empresa[0]["servicio"]; ?>" required/>
                                  </div>
                                   <div class="form-group">
                                      <input class="form-control" name="apellidos" pattern="[a-zA-Z]+" type="text" value="<?php print $empresa[0]["rif"]; ?>" required/>
                                  </div>
                                   <div class="form-group">
                                      <input class="form-control" name="telefono" pattern="[0-9--]+" type="text" value="<?php print $empresa[0]["telefono"]; ?>" required/>
                                  </div>
                                   <div class="form-group">
                                      <input class="form-control" name="fecha" type="text" value="<?php print $empresa[0]["estado"]; ?>" required/>
                                  </div>
                                   <div class="form-group">
                                      <input class="form-control" name="email" type="email" value="<?php print $empresa[0]["id_categoria"]; ?>" required/>
                                  </div>                    
                                </fieldset>
                            </form>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bell fa-fw"></i> Redes Sociales
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="list-group">
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-flag fa-fw"></i> <?php echo $empresa[0]["facebook"]?>
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-flag fa-fw"></i> <?php echo $empresa[0]["twitter"]?>
                                    </span>
                                </a>
                                 <a href="#" class="list-group-item">
                                    <i class="fa fa-flag fa-fw"></i> <?php echo $empresa[0]["correo"]?>
                                </a>
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-flag fa-fw"></i> <?php echo $empresa[0]["instagram"]?>
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-flag fa-fw"></i> <?php echo $empresa[0]["pinterest"]?>
                                    </span>
                                </a>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    
                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>