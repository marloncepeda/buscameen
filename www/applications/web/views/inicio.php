<p align="center"> 
  <img src="<?php print $this->themePath; ?>/img/tu_guia.png">
</p>
<hr>
<div class="login-form">
    <div class="login-content">
      <form method="post" action="<?php print path("web/buscar_empresa/"); ?>" >
      
        <div class="form-group panel">    
          <div class="input-group">
            <div class="input-group-addon">
              <i class="glyphicon glyphicon-th-list"></i>
            </div>
            <select class="form-control" name="estado">
              <option>Elige un Estado</option>
               <option>Todos</option>
              <option>Amazonas</option>
              <option>Anzoategui</option>
              <option>Apure</option>
              <option>Aragua</option>
              <option>Barinas</option>
              <option>Bolivar</option>
              <option>Carabobo</option>
              <option>Cojedes</option>
              <option>Delta Amacuro</option>
              <option>Distrito Capital</option>
              <option>Falcon</option>
              <option>Guarico </option>
              <option>Lara</option>
              <option>Merida</option>
              <option>Monagas</option>
              <option>Nueva Esparta</option>
              <option>Portuguesa</option>
              <option>Sucre</option>
              <option>Tachira</option>
              <option>Trujillo</option>
              <option>Vargas</option>
              <option>Yaracuy</option>
              <option>Zulia</option>
            </select>
          </div>
          <hr>
        </div>
        
        <div class="form-group panel">    
          <div class="input-group">
            <div class="input-group-addon">
              <i class="glyphicon glyphicon-search"></i>
            </div>
            <select class="form-control" name="buscar">
              <option>Elige un tipo de Busqueda</option>
              <option>Establecimientos</option>
              <option>Palabra Clave</option>
            </select>
          </div>
        </div>

        <div class="form-group panel">
          <div class="input-group">
            <div class="input-group-addon">
              <i class="glyphicon glyphicon-th"></i>
            </div>
            <input type="text" class="form-control" name="palabra_clave" placeholder="Busqueda" autocomplete="on" required/>
          </div>
        </div>

        <div class="form-group">
          <input type="submit" class="btn btn-lg btn-success btn-block" name="enviar_btn" value="Buscar">
        </div>
      </form>
    </div>
  </div>
  <hr>