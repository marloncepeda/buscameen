<?php
/**
 * Access from index.php:
 */
if(!defined("_access")) {
	die("Error: You don't have permission to access here...");
}

class web_Model extends ZP_Model {
	
	public function __construct() {
		$this->Db = $this->db();		
		$this->Data = $this->core("Data");
		$this->helpers();
		$this->table_users = "users";
		$this->table_empresa = "empresa";
		$this->table_palabra = "empresa_palabra";
	}
	//Funciones generales del sistema
	public function informacion_agregar(){
		 $data = array(
			"nombre"=>POST("name"),
			"descripcion"=>POST("descripcion"),
			"tipo"=>POST("info_tipo"),
			"autor"=>SESSION('id'),
			"fecha"=>date("d-m-Y")
		);
		$this->Db->insert("informacion", $data);
	}
	public function info_modificar($info_id){
		$data = array(
		 	"nombre"=>POST("name"),
		 	"descripcion"=>POST("descripcion"),
		 	"tipo"=>POST("tipo")
		);
		$this->Db->update("informacion", $data, $info_id);
	}
	public function info_borrar($id){
		$this->Db->delete($id, "informacion");
	}
	public function login($clave){
		$username = POST("usuario");
		$password = $clave;
		$this->Db->table($this->table_users);
		return $data = $this->Db->findBySQL("usuario='$username' AND clave= '$password'");
	}
	
	//funciones de usuarios
	public function registrar($clave){
		 $data = array(
		 	"ci"=>POST("ci"),
		 	"nombre"=>POST("nombres"),
		 	"apellido"=>POST("apellidos"),
		 	"usuario"=>POST("usuario"),
		 	"clave"=>$clave,
			"tipo_user"=>"users",
			"estado"=>"activo",
			"correo"=>POST("email"),
			"fecha_registro"=>date("d-m-Y")
		);
		return $this->Db->insert("users", $data);
	}
	public function modificar_perfil($users_id,$namefile){
		$data = array(
		 	"ci"=>POST("ci"),
		 	"ruta_img"=>$namefile,
		 	"nombre"=>POST("nombres"),
		 	"apellido"=>POST("apellidos"),
		 	"telefono"=>POST("telefono"),
		 	"fecha_nac"=>POST("fecha"),
			"correo"=>POST("email")
		);
		$this->Db->update("users", $data, $users_id);
	}
	public function modificar_clave($user_id){
		$data = array(
		 	"clave"=>POST("clave")
		);
		$this->Db->update("users", $data, $user_id);
	}
	public function buscar_pregunta_usuario($correo){
		$this->Db->table($this->table_users);
		return $data = $this->Db->findBySQL("correo='$correo'");
	}
	public function usuario_borrar($id){
		 $this->Db->delete($id, $this->table_users);
	}
	public function usuario_estado($id,$status){
		$data = array(
		 	"estado"=>$status
		);
		$this->Db->update("users", $data, $id);
	}
	public function buscar_todos_usuarios(){
		return $todos_usuarios = $this->Db->findAll($this->table_users);
	}
	public function logs($accion){
		 $data = array(
		 	"ci_user"=>SESSION("ci"),
		 	"accion"=>$accion,
		 	"fecha_registro"=>date("d-m-Y")
		);
		$this->Db->insert("logs", $data);
	}

	public function buscar_logs($ci_user){
		$this->Db->table($this->table_logs);
		return $data = $this->Db->findBySQL("ci_user='$ci_user' ORDER BY id  DESC LIMIT 10 ");
	}
	
	public function enviar_mensaje_interno(){
		$data = array(
		 	"nombres"=>POST("nombres"),
		 	"email"=>POST("email"),
		 	"telefono"=>POST("telefono"),
		 	"estado"=>POST("estado"),
		 	"mensaje"=>POST("mensaje"),
		 	"fecha"=>date("d-m-Y")
		);
		return $this->Db->insert("mensajes_internos", $data);
	}
	public function buscar_empresa_establecimiento($estado,$palabra_clave){
		//$this->Db->table($this->table_empresa);
		//return $data = $this->Db->findBySQL("estado='$estado' AND nombre_empresa='$palabra_clave'");
		return $data = $this->Db->Query("SELECT * FROM sv_empresa WHERE nombre_empresa like '%$palabra_clave%' AND estado='$estado' AND estatus='Activado' ");
		
	}
	public function buscar_empresa_establecimiento_sin_estado($palabra_clave){
		return $data = $this->Db->Query("SELECT * FROM sv_empresa WHERE nombre_empresa like '%$palabra_clave%' AND estatus='Activado'");
	}
	public function buscar_empresa_palabra_clave($estado,$palabra_clave){
		//$this->Db->table($this->table_palabra);
		//return $data = $this->Db->findBySQL("estado='$estado' AND nombre_empresa like '% $palabra_clave '");
		return $data = $this->Db->Query("SELECT * FROM sv_empresa_palabra WHERE palabra like '%$palabra_clave%' AND estado='$estado' AND estatus='Activado'");
	}
	public function buscar_empresa_palabra_clave_sin_estado($palabra_clave){
		//$this->Db->table($this->table_palabra);
		//return $data = $this->Db->findBySQL("estado='$estado' AND nombre_empresa like '% $palabra_clave '");
		return $data = $this->Db->Query("SELECT * FROM sv_empresa_palabra WHERE palabra like '%$palabra_clave%'");
	}

}
