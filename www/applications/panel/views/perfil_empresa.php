 <div id="page-wrapper">
   
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="entypo-user fa-fw"></i> Datos de Empresa |<a><i class="entypo-pencil"></i></a>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <form action="<?php print $href; ?>" method="post" enctype="multipart/form-data">
                                <fieldset>
                                  <div class="form-group">
                                      <input class="form-control" name="ci" type="text" value="<?php print $empresa[0]["rif"]; ?>" required/>
                                  </div>
      
                                  <div class="form-group">
                                      <input class="form-control" name="nombres" pattern="[a-zA-Z]+" type="text" value="<?php print $empresa[0]["nombre_empresa"]; ?>" required/>
                                  </div>
                                   <div class="form-group">
                                      <input class="form-control" name="telefono" pattern="[0-9--]+" type="text" value="<?php print $empresa[0]["estado"]; ?>" required/>
                                  </div>
                                   <div class="form-group">
                                      <input class="form-control" name="fecha" type="text" value="<?php print $empresa[0]["telefono"]; ?>" required/>
                                  </div>
                                  <div class="form-group">
                                      <input class="form-control" name="email" type="email" value="<?php print $empresa[0]["correo"]; ?>" required/>
                                  </div>
                                  <div class="form-group">
                                      <input class="form-control" name="facebook" type="email" value="<?php print $empresa[0]["facebook"]; ?>" required/>
                                  </div>
                                  <div class="form-group">
                                      <input class="form-control" name="twitter" type="email" value="<?php print $empresa[0]["twitter"]; ?>" required/>
                                  </div>
                                  <div class="form-group">
                                      <input class="form-control" name="instagram" placeholder="instagram" type="text" value="<?php print $empresa[0]["instagram"]; ?>" required/>
                                  </div>
                                  <div class="form-group">
                                      <input class="form-control" name="email" type="text" value="<?php print $empresa[0]["horario"]; ?>" required/>
                                  </div>
                                  <div class="form-group">
                                      <input class="form-control" name="email" type="text" value="<?php print $empresa[0]["servicio"]; ?>" required/>
                                  </div>
                                  <div class="form-group">
                                      <input class="form-control" name="email" type="text" value="<?php print $empresa[0]["estatus"]; ?>" required/>
                                  </div>
                                  <!--<input class="btn btn-lg btn-success btn-block" name="modificar" type="submit" value="Modificar"/>-->                   
                                </fieldset>
                            </form>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bell fa-fw"></i> Palabras Asignadas
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="list-group">
                              <?php 
                                for ($i=0; $i<=$palabras_empresa[$i]["id"]; $i++) { 
                              ?>
                                <a href="#" class="list-group-item">
                                    <i class="entypo entypo-flag"></i> <?php print $palabras_empresa[$i]["palabra"]; ?>
                                    <span class="pull-right text-muted small"><em><?php print $palabras_empresa[$i]["fecha"]; ?></em>
                                    </span>
                                </a>
                              <?php } ?>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>

                    
                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>