<h3 align="center">Registro de Empresas</h3>
<hr>
<form id="rootwizard-2" method="post" action="#" class="form-wizard validate" novalidate="novalidate" enctype="multipart/form-data">
	
	<div class="steps-progress" style="margin-left: 101.5px; margin-right: 101.5px;">
		<div class="progress-indicator" style="width: 0px;"></div>
	</div>
	
	<ul>
		<li class="active">
			<a href="#tab2-1" data-toggle="tab"><span>1</span>Informacion</a>
		</li>
		<li>
			<a href="#tab2-2" data-toggle="tab"><span>2</span>Descripcion</a>
		</li>
		<li>
			<a href="#tab2-3" data-toggle="tab"><span>3</span>Contacto</a>
		</li>
</ul>
	
	<div class="tab-content" style="margin-left: 84px; margin-right: 84px;">
		<div class="tab-pane active" id="tab2-1">
			
				<input class="form-control" name="nombres" type="text" placeholder="Razon Social" required="required" autofocus>
				</br>
				<input class="form-control" name="rif"  type="text" placeholder="RIF" required="required">
				</br>
				<select class="form-control" name="estado">
	              <option>Elige un Estado</option>
	              <option>Amazonas</option>
	              <option>Anzoategui</option>
	              <option>Apure</option>
	              <option>Aragua</option>
	              <option>Barinas</option>
	              <option>Bolivar</option>
	              <option>Carabobo</option>
	              <option>Cojedes</option>
	              <option>Delta Amacuro</option>
	              <option>Distrito Capital</option>
	              <option>Falcon</option>
	              <option>Guarico </option>
	              <option>Lara</option>
	              <option>Merida</option>
	              <option>Monagas</option>
	              <option>Nueva Esparta</option>
	              <option>Portuguesa</option>
	              <option>Sucre</option>
	              <option>Tachira</option>
	              <option>Trujillo</option>
	              <option>Vargas</option>
	              <option>Yaracuy</option>
	              <option>Zulia</option>
	            </select>			
				</br>
				<input class="form-control" name="direccion" type="text" placeholder="Direccion" required="required">
				</br>
				<input class="form-control" name="bookfile" type="file" >logo de empresa</input>
		</div>
		
		<div class="tab-pane" id="tab2-2">
			
			<select class="form-control" name="categoria">
	              <option>Seleccione una Categoria</option>
                    <?php 
                        for($i=0; $i <= $categorias[$i]["id"]; $i++){
                            echo "<option>".$categorias[$i]["nombre"]."<option>";
                    	}
                    ?>
	            </select>			
				</br>
				<input class="form-control" name="horarios" type="text" placeholder="Horarios" required="required">			
				</br>
				<textarea class="form-control" name="servicio" type="text" placeholder="Servicios" required="required">Describe Todos tus Servicios</textarea>		
				</br>
				<select class="form-control" name="tipo_pago">
	              <option>Elige un Tipo de Pago</option>
	              <option>Efectivo</option>
	              <option>Cheque</option>
	              <option>Electronico</option>
	              <option>Transferencias</option>
	              <option>Tarjetas de Credito</option>
	              <option>Tarjetas de Debito</option>
	              <option>Todas</option>
	              <option>Todas menos Cheque</option>
	              <option>Debito, Credito, Efectivo</option>
	              <option>Efectivo, Debito</option>
	              <option>Debito, Credito</option>
	            </select>
				<br />
			
		</div>
		
		<div class="tab-pane" id="tab2-3">
			
			<input class="form-control" name="telefono" type="phone" placeholder="Número Telefónico" required="required">			
			</br>
			<input class="form-control" name="email" type="mail" placeholder="Correo ejem: correo@example.com" required="required">			
			</br>
			<input class="form-control" name="facebook" type="url" placeholder="Facebook ejm: https://facebook.com/usuario" required="required">			
			</br>
			<input class="form-control" name="instagram" type="url" placeholder="instagram ejm: https://instagram.com/usuario" required="required">
			</br>
			<input class="form-control" name="pagina" type="url" placeholder="website ejm: http://dominio.com" required="required">			
			</br>
			<input class="form-control" name="twitter" type="url" placeholder="Twitter ejm: https://twitter.com/usuario#" required="required">			
			</br>

			<input type="submit" class="btn btn-lg btn-primary btn-block large" name="registrar_btn" value="Registrar Empresa" />
			
		</div>
	
		
		<ul class="pager wizard">
			<li class="previous disabled">
				<a href="#"><i class="entypo-left-open"></i> Previous</a>
			</li>
			
			<li class="next">
				<a href="#">Next <i class="entypo-right-open"></i></a>
			</li>
		</ul>
	</div>

</form>