<h2>Todos los Tipos de Informacion Cargadas</h2>

<br />
<a href='#agregar' class='open-add btn btn-success btn-sm btn-icon icon-left'>
  <i class='entypo-plus'></i>
    Agregar
  </a>
<table class="table table-bordered datatable" id="table-1">
    <thead>
        <tr>
            <th>Numero Slider</th>
            <th>Nombre</th>
            <th>Descripcion</th>
            <th>Nombre Imagen</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
    <?php
        for($i=0; $i <= $informacion[$i]["id"]; $i++){
            
                    echo "<tr >";
                    echo "<td>".$informacion[$i]["num_slider"]."</td>";
                    echo "<td>".$informacion[$i]["nombre"]."</td>";
                    echo "<td>".$informacion[$i]["descripcion"]."</td>";
                    echo "<td>".$informacion[$i]["ruta_img"]."</td>";
                    echo "<td>
                        <a data-id='".$informacion[$i]["id"]."' data-tipo='".$informacion[$i]["tipo"]."' data-descripcion='".$informacion[$i]["descripcion"]."' data-nombre='".$informacion[$i]["nombre"]."' href='#editar' class='open-edit btn btn-default btn-sm btn-icon icon-left'>
                            <i class='entypo-pencil'></i>
                            Editar
                        </a>
                        
                        <a data-id='".$informacion[$i]["id"]."' href='#borrar' class='open-borrar btn btn-danger btn-sm btn-icon icon-left'>
                            <i class='entypo-cancel'></i>
                            Borrar
                        </a>
                        
                    </td>";
                    echo "</tr>";
                
            
        }
    ?>
    </tbody>
</table>
</div></div>

<div class="modal fade" id="editar">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Editar Informacion</h4>
            </div>
            
            <div class="modal-body">
            
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        
                        <div class="form-group">
                            <form action="<?php print path("panel/informacion_modificar/") ?>" method="post">
                                <fieldset>
                                    <input type="text" style="visibility: hidden" name="info_id" id="bookId" size="10"value="" />
                                    <div class="form-group">
                                        <input class="form-control" name="name" id="bookname" required/>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" name="descripcion" id="bookdescription"type="text"  required/>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" name="tipo" type="text" id="booktags" required/>
                                    </div>
                                    <input class="btn btn-lg btn-success btn-block" name="modificar" type="submit" value="Modificar"/>                     
                                </fieldset>
                            </form>
                            
                        </div>  
                        
                    </div>
                </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

</div></div>
<div class="modal fade" id="borrar">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Borrar Publicidad</h4>
            </div>
            
            <div class="modal-body">
            
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        
                        <div class="form-group">
                            <p align="center">¿Quieres Borrar la publicidad? una vez Borrado no aparecera mas en el panel ni en el website</p>
                            
                        </div>  
            
                    </div>
                </div>
    
            <div class="modal-footer">
                <form action="<?php print path("panel/informacion_borrar/") ?>" method="post">
                    <input type="text" style="visibility: hidden" name="info_id" id="Id" size="10"value="" />
                    <input type="submit" class="btn btn-default" name="borrar" value="Aceptar"/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </form>
            </div>
        </div>
    </div>
</div>
</div></div>

<div class="modal fade" id="agregar">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Agregar publicidad</h4>
            </div>
            
            <div class="modal-body">
            
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        
                        <div class="form-group">
                            <form action="<?php print path("panel/informacion_agregar/") ?>" enctype="multipart/form-data" method="post">
                                <fieldset>
                                    <div class="form-group" >
                                        <select class="form-control" name="num_slider">
                                            <option >Numero de Slider</option>
                                            <option >0</option>
                                            <option >1</option>
                                            <option >2</option>
                                            <option >3</option>
                                            <option >4</option>
                                            <option >5</option>
                                            <option >6</option>
                                            <option >7</option>
                                            <option >8</option>
                                        </select>   
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="titulo" name="name" id="info_name" required autofocus/>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="descripcion" name="descripcion" id="info_description" type="text"  required/>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" name="userfile" id="info_description" type="file"  required/>
                                    </div>
                                    
                                    <input class="btn btn-lg btn-success btn-block" name="agregar" type="submit" value="Guardar"/>                     
                                </fieldset>
                            </form>
                            
                        </div>  
                        
                    </div>
                </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function($)
    {
        $("#table-1").dataTable({
            "sPaginationType": "bootstrap",
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bStateSave": true
        });
        
        
    });

    jQuery(document).on("click", ".open-edit", function (e) {
        e.preventDefault();
        var _self = $(this);
        var myBookId = _self.data('id');
        var myBookname = _self.data('nombre');
        var myBookdescription = _self.data('descripcion');
        var myBooktags = _self.data('tipo');
        
        $("#bookId").val(myBookId);
        $("#bookname").val(myBookname);
        $("#bookdescription").val(myBookdescription);
        $("#booktags").val(myBooktags);

        $(_self.attr('href')).modal('show',{backdrop: 'static'});
        //alert(myBookId1);
    });


    jQuery(document).on("click", ".open-borrar", function (e) {
        e.preventDefault();
        var _self = $(this);
        var myBookId = _self.data('id');
        $("#Id").val(myBookId);
        $(_self.attr('href')).modal('show',{backdrop: 'static'});
        //alert(myBookId);
    });

    jQuery(document).on("click", ".open-add", function (e) {
        e.preventDefault();
        var _self = $(this);
    
        $(_self.attr('href')).modal('show',{backdrop: 'static'});
    });
</script>