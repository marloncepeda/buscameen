<h2>Todos las Empresas Registradas por <?php print SESSION("nombre")?>, <?php print SESSION("apellido")?></h2>

<br />

<table class="table table-bordered datatable" id="table-1">
    <thead>
        <tr>
            <th>Razon Social</th>
            <th>RIF</th>
            <th>Correo</th>
            <th>Facebook</th>
            <th>Twitter</th>
            <th>Estado</th>
            <th>Horarios</th>
            <th>Telefono</th>
        </tr>
    </thead>
    <tbody>
    <?php
        for($i=0; $i <= $empresas[$i]["id_empresa"]; $i++){
           
            echo "<tr >";
            echo "<td>".$empresas[$i]["nombre_empresa"]."</td>";
            echo "<td>".$empresas[$i]["rif"]."</td>";
            echo "<td>".$empresas[$i]["correo"]."</td>";
            echo "<td>".$empresas[$i]["facebook"]."</td>";
            echo "<td>".$empresas[$i]["twitter"]."</td>";
            if(($empresas[$i]["estatus"]=="Activado")||($empresas[$i]["estatus"]=="activo")){
                echo "<td> <a class='btn btn-lg btn-success'>".$empresas[$i]["estatus"]."</a></td>";
            }else{
                echo "<td> <a class='btn btn-lg btn-danger'>".$empresas[$i]["estatus"]."</a></td>";   
            }
            echo "<td>".$empresas[$i]["horario"]."</td>";
            echo "<td>".$empresas[$i]["telefono"]."</td>";
        }
    ?>
    </tbody>
</table>
</div></div>