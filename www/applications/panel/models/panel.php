<?php
/**
 * Access from index.php:
 */
if(!defined("_access")) {
	die("Error: You don't have permission to access here...");
}

class panel_Model extends ZP_Model {
	
	public function __construct() {
		$this->Db = $this->db();		
		$this->Data = $this->core("Data");
		$this->helpers();
		$this->table_users = "users";
		$this->table_informacion = "informacion";
		$this->table_empresa = "empresa";
		$this->table_mensajes = "mensajes_internos";
		$this->table_palabras = "palabras";
		$this->table_palabras_empresa = "empresa_palabra";
		$this->table_categorias= "categoria";
		$this->table_empresa = "empresa";
	}
	//Funciones generales del sistema
	public function informacion_agregar($namefile){
		 $data = array(
			"num_slider"=>POST("num_slider"),
			"nombre"=>POST("name"),
			"descripcion"=>POST("descripcion"),
			"ruta_img"=>$namefile,
			"autor"=>SESSION('id'),
			"fecha"=>date("d-m-Y")
		);
		$this->Db->insert("informacion", $data);
	}
	public function info_modificar($info_id){
		$data = array(
		 	"nombre"=>POST("name"),
		 	"descripcion"=>POST("descripcion"),
		 	"tipo"=>POST("tipo")
		);
		$this->Db->update("informacion", $data, $info_id);
	}
	public function info_borrar($id){
		$this->Db->delete($id, "informacion");
	}
	public function buscar_todos_informacion(){
		return $toda_informacion = $this->Db->findAll($this->table_informacion);
	}
	public function login($clave){
		$username = POST("usuario");
		$password = $clave;
		$this->Db->table($this->table_users);
		return $data = $this->Db->findBySQL("usuario='$username' AND clave= '$password'");
	}
	
	//funciones de usuarios
	public function registrar($clave){
		 $data = array(
		 	"ci"=>POST("ci"),
		 	"nombre"=>POST("nombres"),
		 	"apellido"=>POST("apellidos"),
		 	"usuario"=>POST("usuario"),
		 	"clave"=>$clave,
			"tipo_user"=>"users",
			"estado"=>"activo",
			"correo"=>POST("email"),
			"fecha_registro"=>date("d-m-Y")
		);
		$this->Db->insert("users", $data);
	}
	public function modificar_perfil($users_id,$namefile){
		$data = array(
		 	"ci"=>POST("ci"),
		 	"ruta_img"=>$namefile,
		 	"nombre"=>POST("nombres"),
		 	"apellido"=>POST("apellidos"),
		 	"telefono"=>POST("telefono"),
		 	"fecha_nac"=>POST("fecha"),
			"correo"=>POST("email")
		);
		$this->Db->update("users", $data, $users_id);
	}
	public function modificar_clave($user_id){
		$data = array(
		 	"clave"=>POST("clave")
		);
		$this->Db->update("users", $data, $user_id);
	}
	public function buscar_pregunta_usuario($correo){
		$this->Db->table($this->table_users);
		return $data = $this->Db->findBySQL("correo='$correo'");
	}
	public function usuario_borrar($id){
		 $this->Db->delete($id, $this->table_users);
	}
	public function usuario_estado($id,$status){
		$data = array(
		 	"estado"=>$status
		);
		$this->Db->update("users", $data, $id);
	}
	public function buscar_todos_usuarios(){
		return $todos_usuarios = $this->Db->findAll($this->table_users);
	}
	public function logs($accion){
		 $data = array(
		 	"ci_user"=>SESSION("ci"),
		 	"accion"=>$accion,
		 	"fecha_registro"=>date("d-m-Y")
		);
		$this->Db->insert("logs", $data);
	}

	public function buscar_logs($ci_user){
		$this->Db->table($this->table_logs);
		return $data = $this->Db->findBySQL("ci_user='$ci_user' ORDER BY id  DESC LIMIT 10 ");
	}
	public function registro_empresa($namefile){
		$data = array(
		 	"estado"=>POST("estado"),
		 	"rif"=>POST("rif"),
		 	"nombre_empresa"=>POST("nombres"),
		 	"id_categoria"=>POST("categoria"),
		 	"correo"=>POST("email"),
		 	"facebook"=>POST("facebook"),
		 	"twitter"=>POST("twitter"),
		 	"instagram"=>POST("instagram"),
		 	"pagina_web"=>POST("pagina"),
		 	"telefono"=>POST("telefono"),
		 	"direccion"=>POST("direccion"),
		 	"logo"=>$namefile,
		 	//"mapa"=>POST(""),
		 	//"permiso"=>POST(""),
		 	"horario"=>POST("horarios"),
		 	"tipop"=>POST("tipo_pago"),
		 	"servicio"=>POST("servicio"),
		 	"estatus"=>"desactivado",
		 	"id_usuario"=>SESSION("id"),
		 	"fecha"=>date("d-m-Y")
		);
		return $this->Db->insert("empresa", $data);	
	}
	public function borrar_empresa($id){
		$this->Db->delete($id, "empresa");

	}
	public function buscar_users_empresas($id){
		$this->Db->table($this->table_empresa);
		return $this->Db->findBySQL("id_usuario='$id'");
	}
	public function buscar_todos_mensajes(){
		return $toda_informacion = $this->Db->findAll($this->table_mensajes);
	}
	public function buscar_todos_empresas(){
		return $toda_informacion = $this->Db->findAll($this->table_empresa);
	}
	public function buscar_todos_empresas_activo(){
		$this->Db->table($this->table_empresa);
		return $data = $this->Db->findBySQL("estatus='Activado'");
	}
	public function empresa_status($id_empresa,$status){
		//$data = array(
		// 	"esstatus"=>$status
		//);
		//$this->Db->update("empresa", $data, $info_id);
		return $data = $this->Db->Query("UPDATE sv_empresa SET estatus='$status' WHERE id_empresa='$id_empresa'");
		
	}
	public function mapas_empresa_agregar($id_empresa,$namefile){
		return $data = $this->Db->Query("UPDATE sv_empresa SET mapa='$namefile' WHERE id_empresa='$id_empresa'");
		
	}
	public function buscar_todos_palabras(){
		return $toda_palabras = $this->Db->findAll($this->table_palabras);
	}
	public function buscar_todas_categorias(){
		return $toda_cateorias = $this->Db->findAll($this->table_categorias);
	}
	public function agregar_palabra_empresa($empresa_id,$palabra){
		$data = array(
		 	"id_empresa"=>$empresa_id,
		 	"palabra"=>$palabra,
		 	"fecha"=>date("d-m-Y")
		);
		return $this->Db->insert("empresa_palabra", $data);	
	}
	public function buscar_empresa_espesifica($id_empresa){
		$this->Db->table($this->table_empresa);
		return $data = $this->Db->findBySQL("id_empresa='$id_empresa'");
	}
	public function buscar_empresa_palabra_espesifica($empresa_id){
		$this->Db->table($this->table_palabras_empresa);
		return $data = $this->Db->findBySQL("id_empresa='$empresa_id'");
	}
	public function buscar_todos_empresas_users($id_usuario){
		$this->Db->table($this->table_empresa);
		return $data = $this->Db->findBySQL("id_usuario='$id_usuario'");	
	}
}
