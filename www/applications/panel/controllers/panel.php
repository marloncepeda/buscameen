<?php
/**
 * Access from index.php:
 */
class panel_Controller extends ZP_Controller {
	
	public function __construct() {
		$this->app("panel");
		$this->Files = $this->core("Files");
		$this->Templates = $this->core("Templates");
		$this->Templates->theme("backend");
		$this->helper("alerts");
		$this->helper("sessions");
		$this->helper("security");
		$this->panel_Model = $this->model("panel_Model");
	}

	
	public function home() {	
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="panelrio")){
			$this->title("Todas las Empresas");
			$vars["empresas"]= $this->panel_Model->buscar_users_empresas(SESSION("id"));
			$vars["view"] = $this->view("home",TRUE);
			$this->render("content",$vars);
				
			$logs = $this->panel_Model->logs("Ver Libros Prestados");
			
		}else{
			$this->index(); 
		}
	}
	public function salir() {	
		$logs = $this->panel_Model->logs("Salir");		
 		unsetSessions($URL);
		$this->index(); 
	}
	public function perfil_usuario(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="panelrio")){
			if(POST("modificar")){
				$uploaddir = '/opt/lampp/htdocs/www/lib/themes/panel/img/';
				$uploadfile = $uploaddir.basename($_FILES['userfile']['name']);
				$namefile = basename($_FILES['userfile']['name']);

				if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
					chmod($uploadfile, 0755);
					$data = $this->panel_Model->modificar_perfil(SESSION("id"),$namefile);
				
					echo "<script>alert('Datos Guardados con Exito Vuelva a Entrar al Sistema');</script>";
					$logs = $this->panel_Model->logs("Modificar Perfil");
					$this->salir();
				} else {
					echo "<script>alert('Error en la carga del archivo'".basename($_FILES['userfile']['name'])."')</script>";
					$this->perfil_usuario();
				}

			}else{
				$data = $this->panel_Model->buscar_logs(SESSION("ci"));
				$vars["logs"] = $data;
				$this->title("Ver Contactos");
				$vars["view"] = $this->view("perfil",TRUE);
				$this->render("content",$vars);
				$logs = $this->panel_Model->logs("Ver Perfil");

			}
		}else{
			$this->login(); 
		}
	}


	public function datos_ver(){
		if(SESSION("tipo_user")=="admin"){
			$data = $this->panel_Model->datos_ubicacion();	
			$vars["datos_ubicacion"] = $data;
			$vars["view"] = $this->view("configuracion/datos_ubicacion",TRUE);
			$this->render("content",$vars);
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}

	public function datos_agregar(){
		if(SESSION("tipo_user")=="admin"){
			if(POST("agregar")){
				$this->panel_Model->datos_agregar();
				//____($data);
				echo "<script>alert('Se ha guardado Correctamente');</script>";
				$this->listas_informacion();
			}else{
				$this->listas_informacion();
			}
		}else{
			$this->login();
		}	
	}
	public function datos_modificar(){
		if(SESSION("tipo_user")=="admin"){
			if(POST("modificar")){
				$data = $this->panel_Model->datos_modificar(POST("datos_id"));
				$this->listas_informacion();
			}
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}
	public function listas_informacion() {
		if(SESSION("tipo_user")=="admin"){
			$data = $this->panel_Model->buscar_todos_informacion();
			if($data =="false"){
				$this->informacion_agregar();
			}else{
				$this->title("Lista Informacion");
				$vars["informacion"] = $data;
				$vars["view"] = $this->view("listas_informacion",TRUE);
				$this->render("content",$vars);
				//$logs = $this->panel_Model->logs("Ver Listas de Informacion");
			}
		}else{
			$this->login(); 
		}
	}
	public function informacion_agregar(){
		if(SESSION("tipo_user")=="admin"){
			if(POST("agregar")){
				$uploaddir = '/opt/lampp/htdocs/www/lib/themes/buscameen/img/slider/';
				$uploadfile = $uploaddir.basename($_FILES['userfile']['name']);
				$namefile = basename($_FILES['userfile']['name']);
				if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
					chmod($uploadfile, 0755);
					$data = $this->panel_Model->informacion_agregar($namefile);
					echo "<script>alert('Datos Guardados con Exito');</script>";
					$this->listas_informacion();
				}else{
					echo "<script>alert('La imagen no fue guardada en sistema intente de nuevo');</script>";
					$this->home();
				}
			

			}else{
				$this->listas_informacion();
			}
		}else{
			$this->login();
		}	
	}
	public function recuperar_clave(){
			if(POST("registro")){
				$data = $this->panel_Model->buscar_pregunta_usuario(POST("correo"));
				if($data[0]["respuesta"]==POST("respuesta")){
					$id=$data[0]["id"];
					$this->panel_Model->modificar_clave($id);
					echo "<script>alert('Se ha Modificado Correctamente');</script>";
					$this->login();
				}else{
					echo "<script>alert('La respuesta no es la misma');</script>";
				}
			}else{
				$this->title("Recuperar Clave");
				$vars["view"] = $this->view("recuperar",TRUE);
				$this->render("content",$vars);
			}	
	}
	public function informacion_modificar(){
		if(SESSION("tipo_user")=="admin"){
			if(POST("modificar")){
				$data = $this->panel_Model->info_modificar(POST("info_id"));
				$this->listas_informacion();
			}
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}
	public function informacion_borrar(){
		if(SESSION("tipo_user")=="admin"){
			if(POST("borrar")){
				$data = $this->panel_Model->info_borrar(POST("info_id"));
				$this->listas_informacion();
			}
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}
	
	public function listas_usuarios() {
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="panelrio")){
			$data = $this->panel_Model->buscar_todos_usuarios();
			//____($data);
			if($data == false){
				echo "<script>alert('no hay usuarios en el sistema'); document.location.href='';</script>";
			}else{
				$this->title("Lista Usuarios");
				$vars["usuarios"] = $data;
				$vars["view"] = $this->view("listas_todos_usuarios",TRUE);
				$this->render("content",$vars);
				$logs = $this->panel_Model->logs("Ver Listas de Usuarios");
			}
		}else{
			$this->login(); 
		}
	}

	public function usuarios_options(){
		if( (SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="panelrio")){
			if(POST("borrar")){
				$data = $this->panel_Model->usuario_borrar(POST("libro_id"));
				echo "<script>alert('usuario Borrado con exito');</script>";
				$logs = $this->panel_Model->logs("Borrar Usuario");	
				$this->listas_usuarios();
			}elseif(POST("bloquear")){
				$data = $this->panel_Model->usuario_estado(POST("libro_id"),"bloqueado");
				$logs = $this->panel_Model->logs("Bloquear Usuario");
				$this->listas_usuarios();
			}elseif(POST("desbloquear")){
				$data = $this->panel_Model->usuario_estado(POST("libro_id"),"activo");
				$logs = $this->panel_Model->logs("Desbloquear Usuario");
				$this->listas_usuarios();
			}
		}else{
			$this->login(); 
		}
	}
	public function usuario_borrar(){
		if(SESSION("tipo_user")=="admin"){
			if(POST("borrar")){
				$data = $this->panel_Model->usuario_borrar(POST("archivo_id"));
				//____($data);
				$logs = $this->panel_Model->logs("borrar contacto");
				echo "<script>alert('Se ha borrrado Correctamente');</script>";
				$this->listas_usuarios();
			}else{
				$this->title("Ver Contactos");
				$vars["view"] = $this->view("contactos/home",TRUE);
				$this->render("content",$vars);
			}
		}else{
			$this->login(); 
		}	
	}

	public function listas_mensajes() {
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="panelrio")){
			$data = $this->panel_Model->buscar_todos_mensajes();
			//____($data);
			if($data == false){
				echo "<script>alert('no hay mensajes en el sistema'); document.location.href='';</script>";
			}else{
				$this->title("Lista Mensajes");
				$vars["mensajes_interno"] = $data;
				$vars["view"] = $this->view("listas_mensajes",TRUE);
				$this->render("content",$vars);
				$logs = $this->panel_Model->logs("Ver Listas de Usuarios");
			}
		}else{
			$this->login(); 
		}
	}

	public function listas_empresas() {
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="panelrio")){
			$data = $this->panel_Model->buscar_todos_empresas();
			//____($data);
			if($data == false){
				echo "<script>alert('no hay mensajes en el sistema'); document.location.href='';</script>";
			}else{
				$this->title("Lista Empresas");
				$vars["empresas"] = $data;
				$vars["palabras"] = $this->panel_Model->buscar_todos_palabras();
				$vars["view"] = $this->view("listas_empresas",TRUE);
				$this->render("content",$vars);
				$logs = $this->panel_Model->logs("Ver Listas de Usuarios");
			}
		}else{
			$this->login(); 
		}
	}

	public function listas_empresas_users(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="panelrio")){
			$data = $this->panel_Model->buscar_todos_empresas_users(SESSION("id"));
			//____($data);
			if($data == false){
				echo "<script>alert('no hay Empresas cargadas en sistemas'); document.location.href='';</script>";
				$this->registro_empresa();
			}else{
				$this->title("Lista Empresas: ".SESSION("nombre"));
				$vars["empresas"] = $data;
				$vars["palabras"] = $this->panel_Model->buscar_todos_palabras();
				$vars["view"] = $this->view("listas_empresas",TRUE);
				$this->render("content",$vars);
				$logs = $this->panel_Model->logs("Ver Listas de Usuarios");
			}
		}else{
			$this->login(); 
		}
	}
	public function empresa_activar(){
		if(SESSION("tipo_user")=="admin"){
			if(POST("activar_btn")){
				$data = $this->panel_Model->empresa_status(POST("empresa_id"),POST("status"));
				$this->listas_empresas();
			}else{ }
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}
	public function agregar_palabra_empresa(){
		if(SESSION("tipo_user")=="admin"){
			if(POST("agregar_palabra_btn")){
				$data = $this->panel_Model->agregar_palabra_empresa(POST("empresa_id"),POST("palabra"));
				$this->listas_empresas();
			}else{ }
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}
	public function ver_perfil_empresa(){
		if(SESSION("tipo_user")=="admin"){
			if(POST("ver_perfil_btn")){
				$vars["empresa"] = $this->panel_Model->buscar_empresa_espesifica(POST("empresa_id"));
				$vars["palabras_empresa"] = $this->panel_Model->buscar_empresa_palabra_espesifica(POST("empresa_id"));
				$vars["view"] = $this->view("perfil_empresa",TRUE);
				$this->render("content",$vars);
			}else{ }
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}
	public function borrar_empresa(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="panelrio")){
			if(POST("borrar_btn")){
				$this->panel_Model->borrar_empresa(POST("empresa_id"));
				$this->home();
			}else{ }
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}
	public function registro_empresas(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="panelrio")){
			/*if(POST("registrar_btn")){	
				$this->panel_Model->registro_empresa();
				echo "<script>alert('La empresa esta Registrada y en espera de aprobación')</script>";
				$this->listas_empresas_users();
			}*/

			if(POST("registrar_btn")){
				if(POST("bookfile")==""){
					echo "<script>alert('La empresa esta Registrada y en espera de aprobación')</script>";
					$data = $this->panel_Model->registro_empresa("logo_buscameen.png");
					$this->home();
				}else{
					$uploaddir = '/opt/lampp/htdocs/www/lib/themes/buscameen/img/logos_empresas/';
					$uploadfile = $uploaddir.basename($_FILES['bookfile']['name']);
					$namefile = basename($_FILES['bookfile']['name']);

					if (move_uploaded_file($_FILES['bookfile']['tmp_name'], $uploadfile)) {
						chmod($uploadfile, 0755);
						$data = $this->panel_Model->registro_empresa($namefile);
						//$logs = $this->biblioteca_Model->logs("Subir Archivo");				    	
	              		echo "<script>alert('La empresa esta Registrada y en espera de aprobación')</script>";
						$this->home();
					} else {
						echo "<script>alert('Error en la carga del archivo'".basename($_FILES['bookfile']['name'])."')</script>";
						//$this->registro_empresa();
					}
				}
				
			}else{
				$vars["categorias"] = $this->panel_Model->buscar_todas_categorias();
				$vars["view"] = $this->view("registro_empresa",TRUE);
				$this->render("content",$vars);
				
				//$logs = $this->panel_Model->logs("Ver Libros Prestados");
			
			}
		}else{
			$this->index(); 
		}
	}
	public function mapa_empresa_agregar(){
		if(SESSION("tipo_user")=="admin"){
			/*if(POST("agregar")){
				$this->panel_Model->mapas_empresa_agregar(POST(),);
				//____($data);
				echo "<script>alert('Se ha guardado Correctamente');</script>";
				$this->listas_empresas();
			}*/
			if(POST("agregar_btn")){
				$uploaddir = '/opt/lampp/htdocs/www/lib/themes/buscameen/img/mapas_empresas/';
				$uploadfile = $uploaddir.basename($_FILES['bookfile']['name']);
				$namefile = basename($_FILES['bookfile']['name']);

				if (move_uploaded_file($_FILES['bookfile']['tmp_name'], $uploadfile)) {
					chmod($uploadfile, 0755);
					$data = $this->panel_Model->mapas_empresa_agregar(POST("empresa_id"), $namefile);
					//$logs = $this->biblioteca_Model->logs("Subir Archivo");				    	
              		echo "<script>alert('Mapa Guardado con Exito')</script>";
					$this->home();
				} else {
					echo "<script>alert('Error en la carga del archivo'".basename($_FILES['bookfile']['name'])."')</script>";
					$this->registro_empresa();
				}
				
			}else{
				$this->listas_empresas();
			}
		}else{
			$this->home();
		}	
	}
	public function respaldar_bd(){
		if(SESSION("tipo_user")=="admin"){
			echo "<script> document.location.href='".get("webURL")."/www/applications/panel/controllers/backupdb.php'; alert('se ha guardado con exito')</script>";
			//$logs = $this->panel_Model->logs("Respaldar BD");
			$this->home();
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}
	public function respaldar_files(){
		if(SESSION("tipo_user")=="admin"){
			exec("tar -zcvf panel_files.tar /opt/lampp/htdocs/www/lib/files/ ");
			echo "<script>alert('El respaldo comprimido de los ficheros se encuenta en la carpeta raiz del sistema');</script>";
			$logs = $this->panel_Model->logs("Respaldar Ficheros");
			echo $this->home();
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}
	public function reporte_usuarios_global(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="panelrio")){	
			echo "<script> document.location.href='http://localhost/panel/www/classes/reporte_usuarios.php';</script>";
			$logs = $this->panel_Model->logs("Reporte Usuarios");
			$this->home();
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}	
	}
	public function reporte_archivos_global(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="panelrio")){
			echo "<script> document.location.href='http://localhost/panel/www/classes/reporte_libros.php';</script>";
			$logs = $this->panel_Model->logs("Reporte Archivos");
			$this->home();
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}
	public function reporte_logs_global(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="panelrio")){
			echo "<script> document.location.href='http://localhost/panel/www/classes/reporte_logs.php';</script>";
			$logs = $this->panel_Model->logs("Reporte Logs");
			$this->home();
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}

}
