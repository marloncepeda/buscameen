<?php 
    if(!defined("_access")) {
        die("Error: You don't have permission to access here..."); 
    }
?>

<!DOCTYPE html>
<html lang="<?php print get("webLang"); ?>">
<head>
  <meta charset="utf-8">
  <title><?php print $this->getTitle(); ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

    <!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
    <!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
    <!--script src="js/less-1.3.3.min.js"></script-->
    <!--append 
#!watch
 to the browser URL, then refresh the page. -->
    
    <link href="<?php print $this->themePath; ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php print $this->themePath; ?>/css/style.css" rel="stylesheet">
    <link href="<?php print $this->themePath; ?>/css/style_slider.css" rel="stylesheet">

      <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
      <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
      <![endif]-->

      <!-- Fav and touch icons -->
    <!--<link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="img/favicon.png">-->
  
   
</head>

<body>
<div class="container-fluid"> 
    <div class="row clearfix ">
        <div class="col-md-12 column">
            <div class="row clearfix">
                <div class="col-md-12 column">
                    <div class="row clearfix">
                        <br />
                        <div class="col-md-8 column">
                            <img src="<?php print $this->themePath; ?>/img/logo.png" />
                        </div>
                        <div class="col-md-2 column">
                        </div>
                        <div class="col-md-2 column">
                            <img src="<?php print $this->themePath; ?>/img/androi.fw.png" />
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <!-- nav begin -->
             <nav id="meuMenu"class="navbar navbar-default fixar" role="navigation" ><!-- style="top:-50px;"-->
                 <div class="container-fluid">
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="dropdown ">
                            <a class=""  href="<?php print path('web/index')?>">
                                Inicio
                                <i class="glyphicon glyphicon-home"></i>  <i class="fa fa-caret-down"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle"  href="<?php print path('web/quienesSomos')?>">
                                Quienes Somos
                                <i class="glyphicon glyphicon-info-sign"></i>  <i class="fa fa-caret-down"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle"  href="<?php print path('web/porqueSubcribirse')?>">
                                Porque Suscribirce
                                <i class="glyphicon glyphicon-question-sign"></i>  <i class="fa fa-caret-down"></i>
                            </a>
                        </li><li class="dropdown">
                            <a class="dropdown-toggle"  href="<?php print path('web/queOfrecemos')?>">
                                Que Ofecemos
                                <i class="glyphicon glyphicon-briefcase"></i>  <i class="fa fa-caret-down"></i>
                            </a>
                        </li><li class="dropdown">
                            <a class="dropdown-toggle"  href="<?php print path('web/contactanos')?>">
                                Contactanos
                                <i class="glyphicon glyphicon-envelope"></i>  <i class="fa fa-caret-down"></i>
                            </a>
                        </li>
                        <?php if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="panelrio")){ ?>
                        <li class="dropdown">
                            <a class="dropdown-toggle"  href="<?php print path('panel/salir')?>">
                                <?php echo SESSION("nombre").", ".SESSION("Apellido")." | Salir"; ?>
                                 <i class="glyphicon glyphicon-lock"></i>  <i class="fa fa-caret-down"></i>
                            </a>
                        </li>
                        <?php }else{ ?>
                        <li class="dropdown">
                            <a class="dropdown-toggle"  href="#mymodal" data-toggle="modal" data-target="#mymodal">
                                Entrar
                                <i class="glyphicon glyphicon-user"></i>  <i class="fa fa-caret-down"></i>
                            </a>
                        </li>
                        </li><li class="dropdown">
                            <a class="dropdown-toggle"  href="#registro" data-toggle="modal" data-target="#registro">
                                Registrar
                                <i class="glyphicon glyphicon-plus"></i>  <i class="fa fa-caret-down"></i>
                            </a>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
                
            </nav>
            <!-- nav end-->
            
            <!-- slider 3d begin -->
            <section id="dg-container" class="dg-container">
                <div class="dg-wrapper">
                    <?php 
                    for ($i=0; $i <= $sliders[$i]["id"] ; $i++) {
                        if($sliders[$i]["num_slider"]=="0"){
                    ?>
                         <a href="#"><img src="<?php print $this->themePath; ?>/img/slider/<?php print $sliders[$i]["ruta_img"]; ?>" alt="image01" height="280px" width="480px"><div><?php print $sliders[$i]["nombre"]; ?></div></a>
                    <?php 
                        }else{} 
                    } 
                    ?>
                    
            </section>
            <!-- Slider 3d fin -->
            <br />
            <br />
            <br />
            <hr>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-md-2 column">

            <div id="myCarousel1" class="carousel slide vertical">
            <!-- Carousel items -->
                <div class="carousel-inner">
                 
                    <div class="active item">
                        <img src="<?php print $this->themePath; ?>/img/slider/publicidad01.fw.png">
                    </div>
                    <div class="item">
                        <img src="<?php print $this->themePath; ?>/img/slider/publicidad05.fw.png">
                    </div>
                </div>
                <!-- Carousel nav -->
                <a class="carousel-control left" href="#myCarousel1" data-slide="prev">‹</a>
                <a class="carousel-control right" href="#myCarousel1" data-slide="next">›</a>
            </div>


            <div id="myCarousel2" class="carousel slide vertical">
            <!-- Carousel items -->
                <div class="carousel-inner">
                    <div class="active item">
                        <img src="<?php print $this->themePath; ?>/img/slider/publicidad02.fw.png">
                    </div>
                    <div class="item">
                        <img src="<?php print $this->themePath; ?>/img/slider/publicidad06.fw.png">
                    </div>
                </div>
                <!-- Carousel nav -->
                <a class="carousel-control left" href="#myCarousel2" data-slide="prev">‹</a>
                <a class="carousel-control right" href="#myCarousel2" data-slide="next">›</a>
            </div>

            <div id="myCarousel3" class="carousel slide vertical">
            <!-- Carousel items -->
                <div class="carousel-inner">
                    <div class="active item">
                        <img src="<?php print $this->themePath; ?>/img/slider/publicidad03.fw.png">
                    </div>
                    <div class="item">
                        <img src="<?php print $this->themePath; ?>/img/slider/publicidad07.fw.png">
                    </div>
                </div>
                <!-- Carousel nav -->
                <a class="carousel-control left" href="#myCarousel3" data-slide="prev">‹</a>
                <a class="carousel-control right" href="#myCarousel3" data-slide="next">›</a>
            </div>

             <div id="myCarousel4" class="carousel slide vertical">
            <!-- Carousel items -->
                <div class="carousel-inner">
                    <div class="active item">
                        <img src="<?php print $this->themePath; ?>/img/slider/publicidad04.fw.png">
                    </div>
                    <div class="item">
                        <img src="<?php print $this->themePath; ?>/img/slider/publicidad08.fw.png">
                    </div>
                </div>
                <!-- Carousel nav -->
                <a class="carousel-control left" href="#myCarousel4" data-slide="prev">‹</a>
                <a class="carousel-control right" href="#myCarousel4" data-slide="next">›</a>
            </div>

        </div>
        
            <div class="modal fade" id="mymodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><p align="center">Entrar</p></h4>
                  </div>
                  <div class="modal-body">
                    <form action="<?php print path('web/login')?>" method="post">
                        <input type="text" class="form-control" name="usuario" placeholder="Usuario" required>
                        <input type="password" class="form-control" name="clave" placeholder="Clave" required>
                        <br />
                        <input type="checkbox" name="recordar"> Recordar Contraseña
                        <br />
                        <br />
                        <input type="submit" class="btn btn-lg btn-primary btn-block" name="entrar_btn" value="Entrar">
                    </form>
                  </div>
                 <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#registro" >Registrar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <div class="modal fade" id="registro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><p align="center">Registro de Usuario</p></h4>
                  </div>
                  <div class="modal-body">
                    <form action="<?php print path('web/registrar')?>" method="post">
                        <input type="text" class="form-control" name="ci" placeholder="Cedula de Identidad" required>
                        <input type="text" class="form-control" name="nombres" placeholder="Nombres" required>
                        <input type="text" class="form-control" name="apellidos" placeholder="Apellidos" required>
                        <input type="mail" class="form-control" name="email" placeholder="Correo Electronico" required>
                        <input type="text" class="form-control" name="usuario" placeholder="Usuario" required>
                        <input type="password" class="form-control" name="clave" placeholder="Clave" required>
                        <br />
                        <input type="checkbox" name="terminos"> Acepto los terminos y condiciones
                        <br />
                        <br />
                        <input type="submit" name="registrar_bnt" class="btn btn-lg btn-primary btn-block" value="Entrar">
                    </form>
                  </div>
                 <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
<?php ?>